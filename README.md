#Artifactory Integration with Bitbucket Pipeline using Artifactory CLI

`To make this integration work you will need to have running Artifactory-pro/Artifactory SAAS/Artifactory Enterprise which is accessible from outside.`

##Steps to Integrate Pipeline with Artifactory.

Step 1:

copy `bitbucket-pipelines.yml` file to your project.

Step 2:

Enable your project in Pipeline.

![screenshot](img/Screen_Shot1.png)

Step 3:

add Environment Variables ARTIFACTORY_URL, ARTIFACTORY_USERNAME and ARTIFACTORY_PASSWORD in build settings of Pipeline.

![screenshot](img/Screen_Shot2.png)

Step 5:

You should be able to see published artifacts in artifactory.

![screenshot](img/Screen_Shot3.png)

Step 6: 

Check build info in build section of Artifactory.

![screenshot](img/Screen_Shot4.png)
